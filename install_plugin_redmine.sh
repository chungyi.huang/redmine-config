#!/bin/bash
plugin=$1
redmine_container_name=`docker ps -a|grep redmine_redmine|awk '{print $NF}'`
docker exec -it  ${redmine_container_name} bundle install
docker exec -it  ${redmine_container_name} bundle exec rake redmine:plugins NAME=${plugin} RAILS_ENV=production
