# redmine-config

Building redmine service through official redmine docker image.

## Getting started

git clone https://gitlab.com/chungyi.huang/redmine-config<br>
cd redmine-config<br>
docker swarm init<br>
./deploy.sh        #(start redmine service.)<br>
./remove.sh        #(stop redmine service.)<br>

## Add redmine plugins(For example: redmine_agile )
cp ~/Downloads/redmine_agile-1_6_2-light.zip . #(Download redmine_agile plugin from https://www.redmineup.com/pages/plugins/agile .) <br>
unzip redmine_agile-1_6_2-light.zip <br>
./cp_plugin_redmine.sh redmine_agile      #(Copy redmine_agile plugin into redmine container.)<br>
./install_plugin_redmine.sh redmine_agile #(Install redmine_agile plugin.)<br>
./restart.sh                              #(Restart redmine service to enable redmine_agile plugin.)<br>

## Setup SMTP for redmine(For example: use localhost simple SMTP service)
modify ./config/configuration.yml<br>
<pre>
default:<br>
  email_delivery:
     delivery_method: :smtp
      smtp_settings:
      address: "localhost"
      port: 25
</pre>
./restart.sh #(Restart redmine service to enable SMTP settings.)
