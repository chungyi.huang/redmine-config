#!/bin/bash
plugin=$1
redmine_container_name=`docker ps -a|grep redmine_redmine|awk '{print $NF}'`
docker cp ${plugin} ${redmine_container_name}:/usr/src/redmine/plugins
